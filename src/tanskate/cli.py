from typer import Typer

from .build_results import build_results
from .count_final import count_final
from .gen_round import gen_round
from .registration_stats import registration_stats

app = Typer()

app.command()(build_results)
app.command()(count_final)
app.command()(gen_round)
app.command()(registration_stats)
