import sys
from collections import OrderedDict, defaultdict
from dataclasses import dataclass
from enum import Enum
from itertools import islice
from math import ceil
from typing import Annotated, Any, Optional

from typer import Option

from .exceptions import TanskateInputError


class ParticipantMarks:
    def __init__(self, marks: list, participants_count: int):
        self.marks = marks
        self.participants_count = participants_count

        self.higher_marks_count = [0] * participants_count
        self.higher_marks_sum = [0] * participants_count

        for mark in marks:
            for i in range(ceil(mark) - 1, participants_count):
                self.higher_marks_count[i] += 1
                self.higher_marks_sum[i] += mark

    def col_equal(self, other, col):
        return (
            self.higher_marks_count[col] == other.higher_marks_count[col]
            and self.higher_marks_sum[col] == other.higher_marks_sum[col]
        )


@dataclass
class Participant:
    start_no: Any
    marks: ParticipantMarks
    meta: list[Any] = None


@dataclass
class RankedParticipant:
    participant: Participant
    rank: float = 0.0


class SharedRank(str, Enum):
    AVERAGE = "AVERAGE"
    TOP = "TOP"


def sort_participants(
    participants: list[Participant],
    marks_threshold,
    start_col=0,
    base_rank=1,
    shared_rank=SharedRank.AVERAGE,
):
    participants_count = participants[0].marks.participants_count

    # TODO: consider not using start_no as index
    pending_participants = {p.start_no: p for p in participants}
    sorted_participants = list()

    for col in range(start_col, participants_count):
        pretendents = list(
            filter(
                lambda p: p.marks.higher_marks_count[col] > marks_threshold,
                pending_participants.values(),
            )
        )

        pretendents.sort(key=lambda p: p.marks.higher_marks_sum[col])
        pretendents.sort(
            key=lambda p: p.marks.higher_marks_count[col],
            reverse=True,
        )

        cluster_start = 0

        for cluster_end in range(1, len(pretendents) + 1):
            if cluster_end < len(pretendents) and pretendents[
                cluster_end
            ].marks.col_equal(
                pretendents[cluster_start].marks,
                col,
            ):
                continue

            cluster_base_rank = len(sorted_participants) + base_rank

            if cluster_end == cluster_start + 1:
                cluster = [
                    RankedParticipant(
                        pretendents[cluster_start],
                        cluster_base_rank,
                    ),
                ]

            else:
                cluster = sort_participants(
                    pretendents[cluster_start:cluster_end],
                    marks_threshold,
                    start_col=col + 1,
                    base_rank=cluster_base_rank,
                    shared_rank=shared_rank,
                )

            sorted_participants.extend(cluster)
            for ranked_p in cluster:
                del pending_participants[ranked_p.participant.start_no]

            cluster_start = cluster_end

    rest = len(pending_participants)

    if rest:
        if shared_rank is SharedRank.AVERAGE:
            shared_rank_offset = sum(range(rest)) / rest
        else:
            shared_rank_offset = 0

        rank = len(sorted_participants) + base_rank + shared_rank_offset
        for p in pending_participants.values():
            sorted_participants.append(RankedParticipant(p, rank))

    return sorted_participants


def calculate_dance_results(participants: list[Participant]):
    judges_count = len(participants[0].marks.marks)
    marks_threshold = judges_count // 2
    return sort_participants(participants, marks_threshold)


def calculate_final_ranks(dance_results: list[list[RankedParticipant]]):
    participants_count = len(dance_results[0])

    participant_places = defaultdict(list)
    participant_marks = defaultdict(list)

    for dance_ranks in dance_results:
        for rp in dance_ranks:
            participant_places[rp.participant.start_no].append(rp.rank)
            participant_marks[rp.participant.start_no].extend(
                rp.participant.marks.marks,
            )

    participants_all_marks = OrderedDict(
        (
            start_no,
            Participant(
                start_no,
                ParticipantMarks(
                    marks,
                    participants_count,
                ),
            ),
        )
        for start_no, marks in participant_marks.items()
    )

    participants = [
        Participant(start_no, ParticipantMarks(places, participants_count))
        for start_no, places in participant_places.items()
    ]
    participants.sort(key=lambda p: p.marks.higher_marks_sum[-1])

    pending_participants = OrderedDict((p.start_no, p) for p in participants)

    ranks = OrderedDict()

    while pending_participants:
        top_p = next(iter(pending_participants.values()))
        top_sum = top_p.marks.higher_marks_sum[-1]
        pretendents = list(
            filter(
                lambda p: p.marks.higher_marks_sum[-1] == top_sum,
                pending_participants.values(),
            )
        )
        ranked_pretendents = sort_participants(
            pretendents,
            marks_threshold=-1,
            start_col=len(ranks),
            base_rank=len(ranks) + 1,
        )

        top_pretendents = list(
            filter(
                lambda p: p.rank == ranked_pretendents[0].rank,
                ranked_pretendents,
            )
        )

        top_pretendents_all_marks = [
            participants_all_marks[p.participant.start_no] for p in top_pretendents
        ]

        ranked_top_pretendents = sort_participants(
            top_pretendents_all_marks,
            marks_threshold=len(top_pretendents_all_marks[0].marks.marks) // 2,
            start_col=len(ranks),
            base_rank=len(ranks) + 1,
            shared_rank=SharedRank.TOP,
        )

        for p in ranked_top_pretendents:
            ranks[p.participant.start_no] = p.rank
            del pending_participants[p.participant.start_no]

    return ranks


def calculate_full_ranks(dance_marks: list[list[Participant]]):
    dance_results = [calculate_dance_results(dance) for dance in dance_marks]
    dance_ranks = defaultdict(list)

    for dance in dance_results:
        for p in dance:
            dance_ranks[p.participant.start_no].append(p.rank)

    final_ranks = calculate_final_ranks(dance_results)

    full_ranks = OrderedDict(
        (start_no, (dance_ranks[start_no], rank))
        for start_no, rank in final_ranks.items()
    )

    return full_ranks


def parse_table(table, dances_count, used_judges_count, meta_cols_count):
    """
    |--------------------------------------------------|
    |          |     |     d_1     | ... |     d_n     |
    | start_no | ... |---------------------------------|
    |          |     | 1 | ... | m | ... | 1 | ... | m |
    |--------------------------------------------------|
    """

    unused_judges_count = (
        len(table[0]) - 1 - meta_cols_count - dances_count * used_judges_count
    ) // dances_count
    total_judges_count = used_judges_count + unused_judges_count

    dance_marks = list()

    for dance_i in range(dances_count):
        participants = list()

        for row in table:
            start_no = row[0]
            meta = row[1 : 1 + meta_cols_count]

            marks_vec_start = 1 + meta_cols_count + dance_i * total_judges_count
            marks_vec_end = marks_vec_start + used_judges_count
            marks_vec = row[marks_vec_start:marks_vec_end]
            marks_vec_int = list(map(int, marks_vec))

            marks = ParticipantMarks(marks_vec_int, len(table))

            participant = Participant(start_no, marks, meta)
            participants.append(participant)

        dance_marks.append(participants)

    return dance_marks, unused_judges_count


def gen_table(dance_marks, unused_judges_count, full_ranks):
    """
    |--------------------------------------------------|-----------------|------|
    |          |     |     d_1     | ... |     d_n     |   dance_ranks   |      |
    | start_no | ... |---------------------------------|-----------------| rank |
    |          |     | 1 | ... | m | ... | 1 | ... | m | d_1 | ... | d_n |      |
    |--------------------------------------------------|-----------------|------|
    """
    padding = [""] * unused_judges_count
    dance_participant_dicts = [
        {participant.start_no: participant for participant in dance}
        for dance in dance_marks
    ]

    table = list()

    for start_no, (dance_ranks, rank) in full_ranks.items():
        row = [start_no] + dance_participant_dicts[0][start_no].meta

        for dance in dance_participant_dicts:
            row += dance[start_no].marks.marks + padding

        row += dance_ranks + [rank]

        row_str = list(map(str, row))
        table.append(row_str)

    return table


def infer_dances_judges_count(
    table: list[list[str]],
    meta_cols_count: int,
    dances_count: Optional[int],
    judges_count: Optional[int],
) -> tuple[int, int]:
    if dances_count is None and judges_count is None:
        raise TanskateInputError(
            "Either '--dances-count', '--judges-count', or both should be present"
        )

    marks_count = len(table[0]) - 1 - meta_cols_count

    if dances_count is None:
        dances_count, rem = divmod(marks_count, judges_count)
        if rem:
            raise TanskateInputError(
                "Failed to infer '--dances-count'. "
                "Please provide it explicitly or check the validity of the marks table"
            )

    elif judges_count is None:
        judges_count, rem = divmod(marks_count, dances_count)
        if rem:
            raise TanskateInputError(
                "Failed to infer '--judges-count'. "
                "Please provide it explicitly or check the validity of the marks table"
            )

    return dances_count, judges_count


# TODO: docs, validation, tests
def count_final(
    dances_count: Annotated[Optional[int], Option()] = None,
    judges_count: Annotated[Optional[int], Option()] = None,
    participants_count: Annotated[Optional[int], Option()] = None,
    meta_cols_count: Annotated[  # TODO: deprecated, remove
        int, Option(hidden=True)
    ] = 0,
):
    """
    Calculate the results of a final round

    Stdin: TSV data from the protocol sheet without ranks and without the header row

    Stdout: TSV data for the protocol sheet with ranks
    """

    lines = (
        sys.stdin
        if participants_count is None
        else islice(sys.stdin, participants_count)
    )

    table = [line.split("\t") for line in lines]

    dances_count, judges_count = infer_dances_judges_count(
        table, meta_cols_count, dances_count, judges_count
    )

    dance_marks, unused_judges_count = parse_table(
        table,
        dances_count,
        judges_count,
        meta_cols_count,
    )

    full_ranks = calculate_full_ranks(dance_marks)

    results_table = gen_table(
        dance_marks,
        unused_judges_count,
        full_ranks,
    )

    for row in results_table:
        print("\t".join(row))
