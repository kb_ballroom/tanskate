from click import UsageError


class TanskateInputError(UsageError):
    pass
