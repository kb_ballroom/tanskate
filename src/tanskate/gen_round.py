from collections.abc import Iterable, Sequence
from dataclasses import dataclass
from itertools import chain
from math import inf
from pathlib import Path
from random import randrange
from typing import Annotated, Optional

from openpyxl import Workbook
from openpyxl.utils.cell import get_column_letter
from openpyxl.worksheet.worksheet import Worksheet
from typer import Option

from . import round_styles as styles
from .config import CategoryConfig, CompetitionConfig, RegistrationConfig
from .exceptions import TanskateInputError
from .registration import RegistrationResults, build_meta_table
from .round_styles import thick, thin

ONE_COLUMN_THRESHOLD = 15
TWO_COLUMNS_THRESHOLD = 30


@dataclass
class Round:
    category: CategoryConfig
    name: str
    choose_for_next_round: int
    heats: list[list[int]]

    def full_name(self) -> str:
        return f"{self.category.name}, {self.name}"

    def is_final(self) -> bool:
        return not self.choose_for_next_round

    def participants_count(self) -> int:
        return sum(map(len, self.heats))

    def judges_count(self) -> int:
        return len(self.category.judges)


def set_column_width(sheet: Worksheet, column_index: int, width: float):
    column_letter = get_column_letter(column_index)
    sheet.column_dimensions[column_letter].width = width


def split_heats(participant_nos: Iterable[int], heats_count: int):
    if heats_count <= 0:
        raise ValueError(f"heats_count should be a positive integer, not {heats_count}")

    participant_nos = list(participant_nos)
    participants_per_heat = len(participant_nos) // heats_count
    heats = []

    for _ in range(heats_count):
        heat = []
        for _ in range(participants_per_heat):
            chosen_index = randrange(len(participant_nos))
            chosen_participant = participant_nos.pop(chosen_index)
            heat.append(chosen_participant)
        heats.append(heat)

    for participant, heat in zip(participant_nos, heats):
        heat.append(participant)

    for heat in heats:
        heat.sort()

    return heats


def map_participant_to_heat(heats: Sequence[Iterable[int]]) -> list[tuple[int, int]]:
    result = []

    for heat_no, heat in enumerate(heats, start=1):
        for participant in heat:
            result.append((participant, heat_no))

    result.sort()
    return result


def split_columns(
    participant_to_heat: list[tuple[int, int]],
    column_thresholds: Sequence[int] = (15, 38),
) -> list[list[tuple[int, int]]]:
    items_count = len(participant_to_heat)
    thresholds_iter = chain(column_thresholds, (inf,))

    columns_count = 1
    while items_count > next(thresholds_iter):
        columns_count += 1

    items_per_column, remainder = divmod(items_count, columns_count)
    items_per_column += bool(remainder)

    columns = []
    start, end = 0, items_per_column

    for _ in range(columns_count):
        column = participant_to_heat[start:end]
        columns.append(column)
        start, end = end, end + items_per_column

    return columns


def fill_judges(
    sheet: Worksheet, start_row: int, start_col: int, judges: Sequence[str]
) -> int:
    sheet.merge_cells(
        start_row=start_row,
        end_row=start_row,
        start_column=start_col,
        end_column=start_col + 1,
    )
    sheet.cell(start_row, start_col, "Судьи:")

    for judge_num, judge in enumerate(judges, start=1):
        row = start_row + judge_num
        sheet.cell(row, start_col, f"{judge_num }.")
        sheet.cell(row, start_col + 1, judge)

    return row


def fill_summary_sheet(
    round: Round,
    sheet: Worksheet,
    reg_results: RegistrationResults,
    reg_config: RegistrationConfig,
):
    # Title

    base_col = 1
    title_row = 1
    sheet.cell(title_row, base_col, round.full_name())

    # Judges

    judges_start_row = title_row + 2
    judges_end_row = fill_judges(
        sheet, judges_start_row, base_col, round.category.judges
    )

    # Heats

    heats_first_row = judges_end_row + 2

    for heat_index, heat in enumerate(round.heats):
        row = heats_first_row + heat_index
        sheet.cell(row, base_col, f"{heat_index + 1} заход:")
        sheet.cell(row, base_col + 1, ", ".join(map(str, heat)))

    # Participants meta

    meta_table_first_row = row + 2

    participant_nos = sorted(chain(*round.heats))
    reg_entries = reg_results.find_entries(participant_nos)
    meta_table = build_meta_table(reg_entries, reg_config.meta_columns)

    for row, row_data in enumerate(meta_table, start=meta_table_first_row):
        for col, cell_data in enumerate(row_data, start=base_col):
            sheet.cell(row, col, cell_data)

    # Dimensions

    last_col = col
    for col in range(base_col, last_col + 1):
        col_letter = get_column_letter(col)
        sheet.column_dimensions[col_letter].best_fit = True


def fill_protocol_sheet(round: Round, sheet: Worksheet):
    upper_header_row = 1
    lower_header_row = 2
    dances_count = len(round.category.dances)

    # Participant nos header

    participant_nos_col = 1
    sheet.merge_cells(
        start_row=upper_header_row,
        end_row=lower_header_row,
        start_column=participant_nos_col,
        end_column=participant_nos_col,
    )
    cell = sheet.cell(upper_header_row, participant_nos_col, "#")
    cell.style = styles.protocol_table_header
    set_column_width(sheet, participant_nos_col, styles.protocol_info_col_width)

    # Marks header

    marks_first_col = participant_nos_col + 1
    marks_cols_count = dances_count * round.judges_count()
    marks_last_col = marks_first_col + marks_cols_count - 1
    marks_first_col_letter = get_column_letter(marks_first_col)
    marks_last_col_letter = get_column_letter(marks_last_col)

    for dance_index, dance_name in enumerate(round.category.dances):
        first_judge_col = marks_first_col + dance_index * round.judges_count()
        last_judge_col = first_judge_col + round.judges_count() - 1

        sheet.merge_cells(
            start_row=upper_header_row,
            end_row=upper_header_row,
            start_column=first_judge_col,
            end_column=last_judge_col,
        )
        cell = sheet.cell(upper_header_row, first_judge_col, dance_name)
        cell.style = styles.protocol_table_header

        for judge_index in range(round.judges_count()):
            col_index = first_judge_col + judge_index
            cell = sheet.cell(
                lower_header_row,
                col_index,
                judge_index + 1,
            )
            cell.style = styles.protocol_table_header
            set_column_width(sheet, col_index, styles.protocol_mark_col_width)

    aggregates_first_col = marks_last_col + 1

    if round.is_final():
        # Dance ranks header

        dance_ranks_first_col = aggregates_first_col
        dance_ranks_last_col = dance_ranks_first_col + dances_count - 1
        sheet.merge_cells(
            start_row=upper_header_row,
            end_row=upper_header_row,
            start_column=dance_ranks_first_col,
            end_column=dance_ranks_last_col,
        )
        cell = sheet.cell(upper_header_row, dance_ranks_first_col, "Место в танце")
        cell.style = styles.protocol_table_header

        for dance_index, dance_name in enumerate(round.category.dances):
            col_index = dance_ranks_first_col + dance_index
            cell = sheet.cell(
                lower_header_row,
                col_index,
                dance_name,
            )
            cell.style = styles.protocol_table_header
            set_column_width(sheet, col_index, styles.protocol_info_col_width)

        rank_col = dance_ranks_last_col + 1
        sheet.merge_cells(
            start_row=upper_header_row,
            end_row=lower_header_row,
            start_column=rank_col,
            end_column=rank_col,
        )
        cell = sheet.cell(upper_header_row, rank_col, "Место")
        cell.style = styles.protocol_table_header
        set_column_width(sheet, rank_col, styles.protocol_info_col_width)

        # Sum header

        sum_col = rank_col + 1
        sum_func = "SUM"

    else:
        sum_col = marks_last_col + 1
        sum_func = "COUNTA"

    sheet.merge_cells(
        start_row=upper_header_row,
        end_row=lower_header_row,
        start_column=sum_col,
        end_column=sum_col,
    )
    cell = sheet.cell(upper_header_row, sum_col, "Сумма")
    cell.style = styles.protocol_table_header
    set_column_width(sheet, sum_col, styles.protocol_info_col_width)

    aggregates_last_col = sum_col

    # Judges

    judges_start_col = aggregates_last_col + 2
    fill_judges(sheet, upper_header_row, judges_start_col, round.category.judges)
    set_column_width(sheet, judges_start_col, styles.protocol_judge_no_col_width)

    # Participant rows

    participants = list(chain(*round.heats))
    participants_first_row = lower_header_row + 1
    participants_last_row = participants_first_row + len(participants) - 1

    for row, participant_no in enumerate(participants, start=participants_first_row):
        cell = sheet.cell(row, participant_nos_col, participant_no)
        cell.style = styles.protocol_table_data
        cell.border = styles.Border(
            top=thin if row == participants_first_row else None,
            right=thin,
            bottom=thin if row == participants_last_row else None,
            left=thin,
        )

        for mark_index in range(dances_count * round.judges_count()):
            cell = sheet.cell(row, marks_first_col + mark_index)
            cell.style = styles.protocol_table_data
            cell.border = styles.Border(
                top=thin if row == participants_first_row else None,
                right=thin if (mark_index + 1) % round.judges_count() == 0 else None,
                bottom=thin if row == participants_last_row else None,
                left=thin if mark_index % round.judges_count() == 0 else None,
            )

        for col in range(aggregates_first_col, aggregates_last_col + 1):
            cell = sheet.cell(row, col)
            cell.style = styles.protocol_table_data
            cell.border = styles.Border(
                top=thin if row == participants_first_row else None,
                right=thin,
                bottom=thin if row == participants_last_row else None,
                left=thin,
            )

        sum_from = f"{marks_first_col_letter}{row}"
        sum_to = f"{marks_last_col_letter}{row}"
        sum_formulae = f"={sum_func}({sum_from}:{sum_to})"
        sheet.cell(row, sum_col, sum_formulae)


def fill_heats_sheet(round: Round, sheet: Worksheet):
    participant_to_heat = map_participant_to_heat(round.heats)
    data_columns = split_columns(participant_to_heat)

    title_row = 1
    cell = sheet.cell(title_row, 1, round.full_name())
    cell.style = styles.heats_title
    sheet.row_dimensions[title_row].height = styles.heats_title_row_height

    column_title_row = title_row + 2
    sheet.row_dimensions[column_title_row].height = styles.heats_column_title_row_height

    heats_table_first_row = column_title_row + 1
    max_col_length = max(map(len, data_columns))
    heats_table_last_row = heats_table_first_row + max_col_length - 1

    for data_column_index, data_column in enumerate(data_columns):
        participant_column_index = data_column_index * 3 + 1
        heat_column_index = participant_column_index + 1
        participant_title_cell = sheet.cell(3, participant_column_index, "#")
        heat_title_cell = sheet.cell(3, heat_column_index, "Заход")

        participant_title_cell.style = heat_title_cell.style = styles.heats_column_title

        for row_index, data_item in enumerate(data_column, start=heats_table_first_row):
            participant_no, heat_no = data_item
            participant_cell = sheet.cell(
                row_index, participant_column_index, participant_no
            )
            heat_cell = sheet.cell(row_index, heat_column_index, heat_no)

            participant_cell.style = styles.heats_participant_no
            heat_cell.style = styles.heats_heat

    for row_index in range(heats_table_first_row, heats_table_last_row + 1):
        sheet.row_dimensions[row_index].height = styles.heats_table_row_height

    cols_used = len(data_columns) * 3 - 1
    for col_index in range(1, cols_used + 1):
        col_letter = get_column_letter(col_index)
        sheet.column_dimensions[col_letter].best_fit = True


def fill_blank_sheet(round: Round, sheet: Worksheet):
    # Header

    header_items = [
        ("Фамилия Имя судьи", "_" * 27),
        ("Категория", round.full_name()),
        ("Участвует", round.participants_count()),
    ]

    if not round.is_final():
        header_items.append(("В следующий тур", round.choose_for_next_round))

    heat_name_col = int(len(round.heats) > 1)

    header_key_first_col = heat_name_col + 1
    header_key_last_col = header_key_first_col + 1
    header_value_col = header_key_last_col + 1
    header_first_row = 1

    for row, header_item in enumerate(header_items, start=header_first_row):
        header_key, header_value = header_item
        sheet.merge_cells(
            start_row=row,
            end_row=row,
            start_column=header_key_first_col,
            end_column=header_key_last_col,
        )
        header_key_cell = sheet.cell(row, header_key_first_col, f"{header_key}:")
        header_value_cell = sheet.cell(row, header_value_col, header_value)

        header_key_cell.style = styles.blank_header_key
        header_value_cell.style = styles.blank_header_value

    header_last_row = row

    # Top table header

    dances_header_row = row + 2
    table_first_col = heat_name_col + 2
    table_last_col = table_first_col + len(round.category.dances) - 1

    for col, dance_name in enumerate(round.category.dances, start=table_first_col):
        cell = sheet.cell(dances_header_row, col, dance_name)
        cell.style = styles.blank_table_top_header

    cell.border = styles.Border(top=thick, right=thick, bottom=thick, left=thin)
    cell = sheet.cell(dances_header_row, table_first_col)
    cell.border = styles.Border(top=thick, right=thin, bottom=thick, left=thick)

    # Table rows

    participant_no_col = heat_name_col + 1
    heat_first_row = dances_header_row + 1

    for heat_num, heat in enumerate(round.heats, start=1):
        # Heat left header, if necessary

        if heat_name_col:
            heat_last_row = heat_first_row + len(heat) - 1
            sheet.merge_cells(
                start_row=heat_first_row,
                end_row=heat_last_row,
                start_column=heat_name_col,
                end_column=heat_name_col,
            )
            heat_name = f"Заход {heat_num}"
            first_cell = sheet.cell(heat_first_row, heat_name_col, heat_name)
            last_cell = sheet.cell(heat_last_row, heat_name_col)

            first_cell.style = last_cell.style = styles.blank_heat_name

        # Participant nos

        for participant_in_heat_index, participant_no in enumerate(heat):
            row = heat_first_row + participant_in_heat_index
            cell = sheet.cell(row, participant_no_col, participant_no)
            cell.style = styles.blank_table_participant_cell

        heat_last_row = row

        # Border around heat data

        for col in range(participant_no_col, table_last_col + 1):
            for row in range(heat_first_row, heat_last_row + 1):
                sheet.cell(row, col).border = styles.Border(
                    top=thick if row == heat_first_row else thin,
                    right=thick if col == table_last_col else thin,
                    bottom=thick if row == heat_last_row else thin,
                    left=thick if col == participant_no_col else thin,
                )

        heat_first_row = heat_last_row + 1

    # Dimensions

    if heat_name_col:
        set_column_width(sheet, heat_name_col, styles.blank_heat_name_col_width)

    for col in range(participant_no_col, table_last_col + 1):
        set_column_width(sheet, col, styles.blank_table_col_width)

    for row in range(dances_header_row, heat_last_row + 1):
        sheet.row_dimensions[row].height = styles.blank_table_row_height

    for row in range(header_first_row, header_last_row + 1):
        sheet.row_dimensions[row].height = styles.blank_header_row_height


def choose_workbook_path(
    out_workbook: Optional[Path], out_dir: Optional[Path], round: Round
) -> Path:
    if out_workbook is not None and out_dir is not None:
        raise TanskateInputError("--out-workbook and --out-dir are mutually exclusive")

    if out_workbook is not None:
        return out_workbook

    out_dir = out_dir or Path.cwd()
    return out_dir / f"{round.full_name()}.xlsx"


# TODO: docs, validation, tests
def gen_round(
    config: Annotated[Path, Option()],
    category_key: Annotated[str, Option()],
    round_name: Annotated[str, Option()],
    participants_file: Annotated[
        Optional[Path],
        Option(
            help=(
                "Newline-separated list of round participants' start numbers. "
                "Omit if this is the first round with all registered participants"
            ),
        ),
    ] = None,
    out_workbook: Annotated[Optional[Path], Option()] = None,
    out_dir: Annotated[Optional[Path], Option()] = None,
    choose: Annotated[
        int,
        Option(
            help=(
                "Number of participants judges will choose for the next round. "
                "0 for the final round"
            )
        ),
    ] = 0,
    heats_count: Annotated[int, Option()] = 1,
):
    """
    Create the spreadsheet for a future round
    """

    comp_config = CompetitionConfig.load(config)
    cat_config = comp_config.categories[category_key]

    reg_results = RegistrationResults.load(comp_config.registration)

    if participants_file is None:
        reg_entries = reg_results.category_entries(category_key)
        participants = [entry.start_no for entry in reg_entries]
    else:
        with open(participants_file) as f:
            participants = list(map(int, f))

    heats = split_heats(participants, heats_count)
    round = Round(
        cat_config,
        round_name,
        choose,
        heats,
    )

    wb = Workbook()
    while wb.worksheets:
        wb.remove(wb.worksheets[0])

    summary_sheet = wb.create_sheet("Сводка")
    protocol_sheet = wb.create_sheet("Протокол")
    heats_sheet = wb.create_sheet("Заходы")
    blank_sheet = wb.create_sheet("Бланк")

    fill_summary_sheet(round, summary_sheet, reg_results, comp_config.registration)
    fill_protocol_sheet(round, protocol_sheet)
    fill_heats_sheet(round, heats_sheet)
    fill_blank_sheet(round, blank_sheet)

    workbook_path = choose_workbook_path(out_workbook, out_dir, round)
    wb.save(workbook_path)
