from pathlib import Path
from typing import Annotated

from typer import Option

from .config import CompetitionConfig
from .registration import RegistrationResults


def registration_stats(config: Annotated[Path, Option()]):
    """
    Show the number of participants per category
    """

    comp_config = CompetitionConfig.load(config)
    reg_results = RegistrationResults.load(comp_config.registration)
    counters = reg_results.category_participants_count()

    max_cat_name_length = max(len(cat.name) for cat in comp_config.categories.values())

    for category_key, category in comp_config.categories.items():
        count = counters.get(category_key, 0)
        print(f"{category.name:.<{max_cat_name_length}} {count}")
