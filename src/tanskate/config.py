from pathlib import Path

from pydantic import BaseModel


class RegistrationConfig(BaseModel):
    results_path: Path
    start_no_column: str
    categories_column: str
    meta_columns: list[str]


class CategoryConfig(BaseModel):
    name: str
    dances: list[str]
    judges: list[str]


class CompetitionConfig(BaseModel):
    registration: RegistrationConfig
    categories: dict[str, CategoryConfig]

    @classmethod
    def load(cls, path: Path) -> "CompetitionConfig":
        return cls.model_validate_json(path.read_text())
