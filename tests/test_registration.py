from pathlib import Path

import pytest

from tanskate.exceptions import TanskateInputError
from tanskate.registration import (
    RegistrationConfig,
    RegistrationEntry,
    RegistrationResults,
    build_meta_table,
)


def test_registration_entry_equality():
    e1 = RegistrationEntry(1, ["1", "2", "3"], {"1", "2"})
    e2 = RegistrationEntry(1, ["1", "2", "3"], {"1", "2"})
    assert e1 == e2

    e3 = RegistrationEntry(1, ["1", "2", "3"], {"1", "3"})
    assert e1 != e3


def test_registration_category_participants_count():
    first = RegistrationEntry(1, ["1", "2"], {"1", "2"})
    second = RegistrationEntry(2, ["1", "2"], {"2", "3"})
    results = RegistrationResults([first, second])
    assert results.category_participants_count() == {"1": 1, "2": 2, "3": 1}


def test_registration_results_category_entries():
    first = RegistrationEntry(1, ["1", "2"], {"1", "2"})
    second = RegistrationEntry(2, ["1", "2"], {"2", "3"})
    third = RegistrationEntry(3, ["1", "2"], {"3", "1"})

    results = RegistrationResults([second, first, third])

    assert results.category_entries("1") == [first, third]
    assert results.category_entries("2") == [first, second]
    assert results.category_entries("3") == [second, third]


def test_registration_results_find_entries():
    first = RegistrationEntry(11, ["1", "2"], {"1", "2"})
    second = RegistrationEntry(22, ["1", "2"], {"2", "3"})
    third = RegistrationEntry(33, ["1", "2"], {"3", "1"})

    results = RegistrationResults([second, first, third])

    assert results.find_entries([33, 11]) == [third, first]
    assert results.find_entries([11, 22]) == [first, second]

    with pytest.raises(KeyError):
        results.find_entries([10, 11])

    with pytest.raises(KeyError):
        results.find_entries([11, 12])

    with pytest.raises(KeyError):
        results.find_entries([11, 111])


def test_load_registration_results(tmp_path: Path):
    table = [
        "Number,Club,Leader Name,Follower Name,Teacher,Categories",
        '2,"Club 2, Club 3",Leader 2,Follower 2,Teacher 2,Cat 2',
        "1,Club 1,Leader 1,Follower 1,Teacher 1,Cat 1",
        ",Club 4,Leader 4,Follower 4,Teacher 4,Cat 1",
        '3,,Leader 3,Follower 3,Teacher 3,"Cat 2, Cat 1"',
    ]

    results_path = tmp_path / "test-results-table.csv"
    results_path.write_text("\n".join(table))

    config = RegistrationConfig(
        results_path=results_path,
        start_no_column="Number",
        categories_column="Categories",
        meta_columns=["Leader Name", "Follower Name", "Club"],
    )

    results = RegistrationResults.load(config)

    assert results.entries == [
        RegistrationEntry(1, ["Leader 1", "Follower 1", "Club 1"], {"Cat 1"}),
        RegistrationEntry(2, ["Leader 2", "Follower 2", "Club 2, Club 3"], {"Cat 2"}),
        RegistrationEntry(3, ["Leader 3", "Follower 3", ""], {"Cat 1", "Cat 2"}),
    ]

    broken_config = RegistrationConfig(
        results_path=results_path,
        start_no_column="Number",
        categories_column="Categories",
        meta_columns=["Leader Name", "Follower Name", "club"],
    )

    with pytest.raises(TanskateInputError):
        RegistrationResults.load(broken_config)


def test_build_meta_table():
    first = RegistrationEntry(2, ["1", "2"], {"1", "2"})
    second = RegistrationEntry(1, ["3", "4"], {"2", "3"})
    table = build_meta_table([first, second], ["Meta 1", "Meta 2"])

    assert table == [
        ["#", "Meta 1", "Meta 2"],
        [2, "1", "2"],
        [1, "3", "4"],
    ]
