import json
import os
from pathlib import Path

from typer.testing import CliRunner

from tanskate.cli import app

run = CliRunner().invoke


def test_e2e(tmp_path: Path):
    # Prepare initial files

    os.chdir(tmp_path)
    (tmp_path / "configs").mkdir()
    (tmp_path / "workbooks").mkdir()

    config = {
        "registration": {
            "results_path": "configs/reg.csv",
            "start_no_column": "Start-No",
            "categories_column": "Categories",
            "meta_columns": [
                "Names",
                "Team",
            ],
        },
        "categories": {
            "am-st": {
                "name": "Amateur Standard",
                "dances": ["W", "T", "V", "F", "Q"],
                "judges": ["Judge 1", "Judge 2", "Judge 3"],
            },
            "pro-st": {
                "name": "Professional Standard",
                "dances": ["W", "T", "V", "F", "Q"],
                "judges": ["Judge 2", "Judge 3", "Judge 4"],
            },
            "pro-am-st": {
                "name": "Pro Am Standard",
                "dances": ["W", "T", "V", "F", "Q"],
                "judges": ["Judge 1", "Judge 2", "Judge 3"],
            },
            "am-la": {
                "name": "Amateur Latin",
                "dances": ["C", "S", "R", "P", "J"],
                "judges": ["Judge 3", "Judge 4", "Judge 1"],
            },
            "pro-la": {
                "name": "Professional Latin",
                "dances": ["C", "S", "R", "P", "J"],
                "judges": ["Judge 4", "Judge 1", "Judge 2"],
            },
            "pro-am-la": {
                "name": "Pro Am Latin",
                "dances": ["W", "T", "V", "F", "Q"],
                "judges": ["Judge 2", "Judge 3", "Judge 4"],
            },
        },
    }

    with open(tmp_path / "configs" / "config.json", "w") as f:
        json.dump(config, f)

    reg_table = [
        "Names,Team,Categories,Start-No",
        'Bob - Alice,Team Bob,"pro-st,pro-la",22',
        'James - Mary,Team James,"pro-st,pro-la",123',
        'Paul - Emma,Team Bob,"am-st,am-la",41',
        'Rob - Helen,Team James,"am-st,am-la",',
        'John - Betty,Team Bob,"am-st",18',
        "Richard - Barbara,Team James,pro-st,83",
        'Charlie - Dorothy,Team Bob,"pro-la,pro-st",26',
        "Donald - Joan,Team James,am-la,36",
        "George - Shirley,Team Bob,pro-st,",
        "Tom - Nancy,Team James,pro-st,74",
    ]

    with open(tmp_path / "configs" / "reg.csv", "w") as f:
        print(*reg_table, sep="\n", file=f)

    # Get help

    result = run(app, ["--help"])
    assert result.exit_code == 0
    assert result.stdout.startswith("Usage: ")

    # Check registration stats

    expected_reg_stats = [
        "Amateur Standard..... 2",
        "Professional Standard 5",
        "Pro Am Standard...... 0",
        "Amateur Latin........ 2",
        "Professional Latin... 3",
        "Pro Am Latin......... 0",
    ]
    expected_stats_out = "\n".join(expected_reg_stats) + "\n"

    result = run(app, ["registration-stats", "--config", "configs/config.json"])
    assert result.exit_code == 0
    assert result.stdout == expected_stats_out

    # Create first round from registration results

    result = run(
        app,
        [
            "gen-round",
            "--config",
            "configs/config.json",
            "--out-dir",
            "workbooks",
            "--category-key",
            "pro-st",
            "--round-name",
            "Semifinal",
            "--choose",
            "3",
            "--heats-count",
            "2",
        ],
    )

    assert result.exit_code == 0
    assert result.stdout == ""
    # TODO: check contents
    assert (tmp_path / "workbooks" / "Professional Standard, Semifinal.xlsx").is_file()

    # Create final round from participants list

    with open("in.tsv", "w") as f:
        print(26, 22, 123, sep="\n", file=f)

    result = run(
        app,
        [
            "gen-round",
            "--config",
            "configs/config.json",
            "--out-workbook",
            "workbooks/Pro St Final.xlsx",
            "--participants-file",
            "in.tsv",
            "--category-key",
            "pro-st",
            "--round-name",
            "Final",
        ],
    )

    assert result.exit_code == 0
    assert result.stdout == ""
    # TODO: check contents
    assert (tmp_path / "workbooks" / "Pro St Final.xlsx").is_file()

    # Count final marks

    in_marks = [
        [22, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 3, 1, 1, 1, 1],
        [26, 2, 1, 1, 2, 1, 1, 3, 3, 3, 1, 1, 2, 2, 2, 2],
        [123, 3, 3, 3, 3, 3, 3, 2, 1, 2, 3, 2, 3, 3, 3, 3],
    ]

    out_marks = [
        in_marks[1] + [1, 1, 3, 1, 2, 1],
        in_marks[0] + [2, 2, 1, 2, 1, 2],
        in_marks[2] + [3, 3, 2, 3, 3, 3],
    ]

    in_marks_str = "\n".join("\t".join(map(str, row)) for row in in_marks) + "\n"
    out_marks_str = "\n".join("\t".join(map(str, row)) for row in out_marks) + "\n"

    result = run(
        app,
        [
            "count-final",
            "--judges-count",
            "3",
        ],
        input=in_marks_str,
    )

    assert result.exit_code == 0
    assert result.stdout == out_marks_str

    # Build final results

    out_results = [
        ["Professional Standard"],
        [],
        ["Место", "#", "Names", "Team"],
        [1, 26, "Charlie - Dorothy", "Team Bob"],
        [2, 22, "Bob - Alice", "Team Bob"],
        [3, 123, "James - Mary", "Team James"],
    ]
    out_results_str = "\n".join("\t".join(map(str, row)) for row in out_results) + "\n"

    result = run(
        app,
        [
            "build-results",
            "--config",
            "configs/config.json",
            "--category-key",
            "pro-st",
        ],
        input=out_marks_str,
    )

    assert result.exit_code == 0
    assert result.stdout == out_results_str
