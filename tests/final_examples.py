from tanskate.count_final import Participant, ParticipantMarks, RankedParticipant

examples = {
    "А": {
        "start_nos": [10, 16, 24, 31, 45, 48],
        "marks": [
            [3, 3, 3, 2, 3],
            [6, 6, 6, 6, 5],
            [2, 2, 5, 4, 1],
            [4, 4, 2, 3, 4],
            [1, 5, 1, 1, 2],
            [5, 1, 4, 5, 6],
        ],
        "results": [3, 6, 2, 4, 1, 5],
    },
    "Б": {
        "start_nos": [11, 21, 32, 41, 51, 61],
        "marks": [
            [1, 5, 1, 1, 2],
            [2, 2, 5, 4, 1],
            [3, 3, 3, 2, 3],
            [4, 4, 2, 3, 4],
            [5, 1, 4, 5, 5],
            [6, 6, 6, 6, 6],
        ],
        "results": [1, 2, 3, 4, 5, 6],
    },
    "В": {
        "start_nos": [12, 22, 32, 42, 52, 62],
        "marks": [
            [1, 1, 1, 4, 4],
            [3, 2, 2, 1, 1],
            [2, 5, 5, 2, 2],
            [4, 3, 4, 5, 3],
            [5, 4, 3, 3, 5],
            [6, 6, 6, 6, 6],
        ],
        "results": [1, 2, 3, 4, 5, 6],
    },
    "Г": {
        "start_nos": [12, 23, 34, 45, 56, 67],
        "marks": [
            [3, 1, 5, 3, 1, 2, 3],
            [1, 4, 1, 1, 2, 1, 2],
            [4, 2, 2, 2, 3, 3, 4],
            [6, 5, 4, 6, 4, 6, 5],
            [2, 3, 3, 4, 5, 5, 1],
            [5, 6, 6, 5, 6, 4, 6],
        ],
        "results": [2, 1, 3, 5, 4, 6],
    },
    "Д": {
        "start_nos": [15, 26, 37, 48, 59, 70],
        "marks": [
            [4, 6, 6, 6, 6],
            [5, 5, 1, 1, 1],
            [6, 1, 3, 3, 4],
            [1, 4, 2, 2, 5],
            [2, 2, 5, 5, 2],
            [3, 3, 4, 4, 3],
        ],
        "results": [6, 1, 4, 2, 3, 5],
    },
    "Е": {
        "start_nos": [13, 23, 33, 43, 53, 63],
        "marks": [
            [1, 1, 1, 5, 5],
            [2, 2, 5, 1, 4],
            [5, 5, 2, 2, 2],
            [3, 3, 4, 6, 1],
            [4, 4, 3, 3, 3],
            [6, 6, 6, 4, 6],
        ],
        "results": [1, 2, 3, 4, 5, 6],
    },
    "Ж": {
        "start_nos": [10, 11, 12, 13, 14, 15],
        "marks": [
            [5, 3, 5, 4, 6, 5, 2],
            [3, 1, 4, 3, 5, 1, 1],
            [1, 4, 2, 2, 2, 3, 4],
            [2, 2, 3, 1, 4, 2, 3],
            [4, 5, 1, 5, 1, 4, 6],
            [6, 6, 6, 6, 3, 6, 5],
        ],
        "results": [5, 3, 2, 1, 4, 6],
    },
    "З": {
        "start_nos": [14, 24, 34, 44, 54, 64],
        "marks": [
            [2, 1, 5, 1, 1],
            [1, 2, 2, 5, 5],
            [5, 6, 1, 2, 2],
            [3, 3, 3, 3, 6],
            [4, 4, 4, 6, 4],
            [6, 5, 6, 4, 3],
        ],
        "results": [1, 2, 3, 4, 5, 6],
    },
    "И": {
        "start_nos": [16, 17, 18, 19, 20, 21],
        "marks": [
            [4, 3, 5, 3, 2],
            [3, 2, 2, 4, 1],
            [2, 1, 1, 5, 4],
            [5, 4, 3, 2, 3],
            [1, 5, 4, 1, 6],
            [6, 6, 6, 6, 5],
        ],
        "results": [3.5, 2, 1, 3.5, 5, 6],
    },
    "К": {
        "start_nos": [81, 82, 83, 84, 85, 86, 87],
        "marks": [
            [1, 2, 3, 4, 5, 6, 7],
            [2, 3, 4, 5, 6, 7, 1],
            [3, 4, 5, 6, 7, 1, 2],
            [4, 5, 6, 7, 1, 2, 3],
            [5, 6, 7, 1, 2, 3, 4],
            [6, 7, 1, 2, 3, 4, 5],
            [7, 1, 2, 3, 4, 5, 6],
        ],
        "results": [4, 4, 4, 4, 4, 4, 4],
    },
    "Л": {
        "start_nos": [30, 31, 32, 33, 34, 35],
        "marks": [
            [1, 6, 1, 6, 1],
            [2, 5, 2, 5, 3],
            [3, 4, 3, 4, 4],
            [4, 3, 4, 3, 6],
            [5, 2, 5, 2, 5],
            [6, 1, 6, 1, 2],
        ],
        "results": [1, 3, 4, 6, 5, 2],
    },
    "М": {
        "start_nos": [17, 27, 37, 47, 57, 67],
        "marks": [
            [1, 1, 2, 5],
            [3, 3, 1, 2],
            [2, 5, 4, 4],
            [4, 2, 3, 6],
            [6, 6, 5, 1],
            [5, 4, 6, 3],
        ],
        "results": [1, 2, 4, 3, 6, 5],
    },
    "Н": {
        "start_nos": [30, 31, 32, 33, 34, 35],
        "marks": [
            [5, 5.5, 2, 5],
            [4, 3.5, 6, 4],
            [2, 2, 3, 1],
            [3, 1, 1, 3],
            [1, 5.5, 4, 6],
            [6, 3.5, 5, 2],
        ],
        "results": [6, 5, 2, 1, 3, 4],
    },
    "О": {
        "start_nos": [71, 72, 73, 74, 75, 76],
        "dances": [
            [
                [2, 6, 4, 4, 5],
                [3, 5, 2, 1, 1],
                [5, 2, 5, 2, 2],
                [4, 4, 1, 6, 6],
                [1, 1, 3, 5, 4],
                [6, 3, 6, 3, 3],
            ],
            [
                [6, 6, 6, 6, 6],
                [4, 3, 2, 2, 1],
                [2, 2, 1, 3, 5],
                [5, 5, 5, 5, 2],
                [3, 1, 3, 1, 3],
                [1, 4, 4, 4, 4],
            ],
            [
                [1, 4, 4, 5, 4],
                [5, 1, 5, 2, 2],
                [2, 5, 2, 1, 1],
                [4, 6, 6, 4, 6],
                [3, 2, 3, 6, 5],
                [6, 3, 1, 3, 3],
            ],
            [
                [5, 4, 3, 3, 6],
                [1, 6, 2, 6, 2],
                [2, 5, 1, 5, 1],
                [6, 3, 6, 4, 4],
                [4, 2, 4, 2, 3],
                [3, 1, 5, 1, 5],
            ],
        ],
        "dance_results": [
            [6, 1, 2, 5, 3, 4],
            [6, 1, 2, 5, 3, 4],
            [5, 2, 1, 6, 4, 3],
            [5, 2, 1, 6, 4, 3],
        ],
        "results": [5, 2, 1, 6, 3, 4],
    },
    "П": {
        "start_nos": [1, 2],
        "dances": [
            [
                [1, 1, 1, 2, 2],
                [2, 2, 2, 1, 1],
            ],
            [
                [2, 2, 2, 1, 1],
                [1, 1, 1, 2, 2],
            ],
            [
                [1, 1, 1, 1, 1],
                [2, 2, 2, 2, 2],
            ],
            [
                [2, 2, 2, 2, 2],
                [1, 1, 1, 1, 1],
            ],
        ],
        "dance_results": [
            [1, 2],
            [2, 1],
            [1, 2],
            [2, 1],
        ],
        "results": [1, 1],
    },
}


def to_participant_list(example):
    participants_count = len(example["start_nos"])
    participants = [
        RankedParticipant(
            Participant(
                start_no,
                ParticipantMarks(marks_vec, participants_count),
            ),
            rank,
        )
        for start_no, marks_vec, rank in zip(
            example["start_nos"], example["marks"], example["results"]
        )
    ]
    return participants


def to_dance_results_list(example):
    participants_count = len(example["start_nos"])
    dances_count = len(example["marks"][0])
    dance_results = list()

    for j in range(dances_count):
        ranks = [example["marks"][i][j] for i in range(participants_count)]
        dance_ranked_participants = [
            RankedParticipant(
                Participant(
                    start_no,
                    marks=ParticipantMarks([], participants_count),
                ),
                rank,
            )
            for start_no, rank in zip(example["start_nos"], ranks)
        ]
        dance_results.append(dance_ranked_participants)

    return dance_results


def to_full_results_list(example):
    participants_count = len(example["start_nos"])
    dance_results = list()

    for marks, ranks in zip(example["dances"], example["dance_results"]):
        participants = [
            RankedParticipant(
                Participant(
                    start_no,
                    ParticipantMarks(marks_vec, participants_count),
                ),
                rank,
            )
            for start_no, marks_vec, rank in zip(example["start_nos"], marks, ranks)
        ]
        dance_results.append(participants)

    return dance_results


def to_full_marks_list(example):
    participants_count = len(example["start_nos"])
    dance_marks = list()

    for marks in example["dances"]:
        participants = [
            Participant(
                start_no,
                ParticipantMarks(marks_vec, participants_count),
            )
            for start_no, marks_vec in zip(example["start_nos"], marks)
        ]
        dance_marks.append(participants)

    return dance_marks
