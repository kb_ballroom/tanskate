# Tanskate

A CLI utility that partially automates scrutineering a ballroom competition.

## Installation

1. Install Python and Poetry.

1. Clone this repository and `cd` into it.

1. Install the project and its dependencies.

    ```shell
    poetry install
    ```

1. In every new terminal activate the virtual environment before running any Tanskate commands.

    ```shell
    poetry shell
    ```

## Usage

See the list of commands:

```shell
tanskate --help
```

Each command automates an aspect of the competition. See the description of a command:

```shell
tanskate <command> --help
```

A competition should be configured with a JSON file that is passed to most commands in the `--config` option. The configuration file also refers to a CSV file with the list of registered participants. See example configuration and the general Tanskate usage flow in [`test_e2e.py`](tests/test_e2e.py).

## Development

Please make sure to lint, format, and test your code before committing. The expected test coverage is 100%.

```shell
ruff .
black .
coverage run
coverage report
```